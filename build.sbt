name := """play-scala-starter-example"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

resolvers += Resolver.sonatypeRepo("snapshots")

scalaVersion := "2.12.8"

libraryDependencies += guice
libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "4.0.1" % Test
libraryDependencies += "com.h2database" % "h2" % "1.4.197"
libraryDependencies += "org.gnieh" %% "sohva" % "2.2.0" intransitive()
   libraryDependencies += "com.typesafe.akka" %% "akka-http-spray-json" % "10.1.7" //10.0.15"
   //libraryDependencies += "io.spray" %% "spray-json" % "1.3.5"
//exclude("com.typesafe.akka","akka-http-spray-json") exclude("com.typesafe.akka","akka-protobuf") exclude("com.typesafe.akka","akka-actor") exclude("com.typesafe.akka","akka-slf4j") exclude("com.typesafe.akka","akka-stream") exclude("com.typesafe.akka","akka-stream-testkit")
//  %% "akka-http-spray-json" % "10.1.7" 10.0.11)

scalacOptions ++= Seq(
  "-feature",
  "-deprecation",
  "-Xfatal-warnings"
)
