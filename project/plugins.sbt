// The Play plugin
addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.7.0")
// fix 'Could not find any member to link for' on 'sbt start'
addSbtPlugin("com.thoughtworks.sbt-api-mappings" % "sbt-api-mappings" % "latest.release")
// Dependency tree
addSbtPlugin("net.virtual-void" % "sbt-dependency-graph" % "0.9.2")