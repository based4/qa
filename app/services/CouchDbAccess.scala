package app.services

// Sohva CouchDB library - http://sohva.gnieh.org/basic/
import gnieh.sohva._
import SohvaProtocol._
import akka.actor.ActorSystem
import akka.util.Timeout
import com.typesafe.config.ConfigFactory

import scala.concurrent.duration._

// https://stackoverflow.com/questions/10472758/scala-how-to-initialize-an-object-using-default-values
case class Candidate(_id: String, var `type`: String = model.Types.CANDIDATE, var name: String, status: String)  extends IdRev

object CouchDbAccess extends App {
  implicit val system = ActorSystem("sohva-system")
  implicit val timeout = Timeout(5.seconds)
  implicit val postFormat = couchFormat[Candidate]

  // https://github.com/lightbend/config#standard-behavior load application.conf
  ConfigFactory.load()

  val couch = new CouchClient
  val db = couch.database("db_name")

  val candidate1 = Candidate("1", model.Types.CANDIDATE, "first", "new")
  val candidate2 = Candidate("2", model.Types.CANDIDATE, "second", "new")

 // for {
  // https://docs.scala-lang.org/tour/for-comprehensions.html
   val savedCandidate1 = db.saveDoc(candidate1)
   val savedCandidate2 = db.saveDoc(candidate2)
   val p1 = db.getDocById[Candidate]("1")
  // val p2 = db.deleteDoc(savedCandidate2)
 // }

  println("Test view accesses:")

  val candidatesDesign = db.design("candidates")
  val candidatesAllView = candidatesDesign.view("all")
 // val candidatesView = candidatesDesign.view("byType")
  //candidatesView.query[Key, Value, Candidate](keys = List(savedCandidate1, savedCandidate2))
  println("All candidates view:" + candidatesAllView.toString)

  println("Finished testing couchDB.")
}

/* Notes:
 1) [ManifestInfo(akka://sohva-system)] Detected possible incompatible versions on the classpath.
 Please note that a given Akka version MUST be the same across all modules of Akka that you are using, e.g.
 if you use [2.5.19] all other modules that are released together MUST be of the same version.
  Make sure you're using a compatible set of libraries. Possibly conflicting versions [2.5.19, 2.5.1]
  in libraries [akka-protobuf:2.5.19, akka-actor:2.5.19, akka-slf4j:2.5.19, akka-stream:2.5.19, akka-stream-testkit:2.5.1]*

  > libraryDependencies += "org.gnieh" %% "sohva" % "2.2.0" intransitive()
   libraryDependencies += "com.typesafe.akka" %% "akka-http-spray-json" % "10.1.7" //10.0.15"
   //libraryDependencies += "io.spray" %% "spray-json" % "1.3.5"
//exclude("com.typesafe.akka","akka-http-spray-json") exclude("com.typesafe.akka","akka-protobuf") exclude("com.typesafe.akka","akka-actor") exclude("com.typesafe.akka","akka-slf4j") exclude("com.typesafe.akka","akka-stream") exclude("com.typesafe.akka","akka-stream-testkit")
//  %% "akka-http-spray-json" % "10.1.7" 10.0.11)

 2) [sohva-system-akka.actor.default-blocking-io-dispatcher-8]
 [akka://sohva-system/system/IO-DNS/inet-address/$a] No caching TTL defined. Using default value Ttl(30 seconds).
 https://github.com/akka/akka/issues/26167 https://github.com/akka/akka/pull/26249
 -> TODO update Akka libraries in Sohva or exclude log WARN for sohva-system-akka.actor - https://github.com/gnieh/sohva
 https://doc.akka.io/docs/akka/current/general/configuration.html#configuration

        # To set the time to cache name resolutions
        # Possible values:
        # default: sun.net.InetAddressCachePolicy.get() and getNegative()
        # forever: cache forever
        # never: no caching
        # n [time unit]: positive timeout with unit, for example "30 s"
        positive-ttl = default
        negative-ttl = default
*/